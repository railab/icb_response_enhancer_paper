#! /bin/bash

set -euo pipefail

# see https://www.unix.com/unix-for-beginners-questions-and-answers/270526-awk-count-unique-element-array.html

cat  *add_name_peaks.bed \
| sort-bed - \
| bedmap --echo --echo-map-id-uniq --delim '\t' - \
| gawk -v OFS="\t" '{a=gensub(/_peak[0-9]+/, "", "g",  $5); $5=a; print}' \
| awk '{split(x,C); n=split($5,F,/;/); for(i in F) if(C[F[i]]++) n--; print $0 "\t" n}'
