#created 20221214 to 
#1. update TCGA multivariate analysis by adding pathology stage
#2. run multivariate analysis for MDA cohort

###PART1-------------------------------------------------------------
#TCGA cohort
#path to the directory where the tcga df is stored 
file_path = "/Users/jiajiachen/Dropbox (Partners HealthCare)/Liu Lab + BRD4/additional analysis/"

#read in tcga dataframe
tcga_df <- read_tsv(file = paste0(file_path,"20221214/Data/","TCGA_SKCM_OS_BRD4_final.tsv"))
colnames(tcga_df)
names(table(tcga_df$ajcc_pathologic_stage))
names(table(tcga_df$pathology_stage))
#specify the order of primary/metastasis status by converting them into factors

#combine stage subgroups to avoid a sub-category size from becoming too small
Stage0 <- c("Stage 0")
StageI <- c("Stage I","Stage IA","Stage IB")
StageII <- c("Stage II","Stage IIA","Stage IIB","Stage IIC")
StageIII <- c("Stage III","Stage IIIA","Stage IIIB","Stage IIIC")
StageIV <- c("Stage IV")

tcga_df <- tcga_df %>%
  mutate(`Primary_Metastasis` = factor(Primary_Metastasis,
                                       levels = c("Primary","Metastatic"))) %>%
  mutate(pathology_stage = if_else(ajcc_pathologic_stage == "'--","Not Reported",ajcc_pathologic_stage)) %>%
  mutate(pathology_stage = case_when(pathology_stage %in% Stage0 ~ "Stage 0",
                                     pathology_stage %in% StageI ~ "Stage I",
                                     pathology_stage %in% StageII ~ "Stage II",
                                     pathology_stage %in% StageIII ~ "Stage III",
                                     pathology_stage %in% StageIV ~ "Stage IV",
                                     pathology_stage == "Not Reported" ~ "Not Reported")) %>%
  filter(pathology_stage != "Not Reported") %>%   #exclude patients with unknown pathology stage
  mutate(`pathology stage` = pathology_stage) %>% #renaming to make them look cleaner in table
  mutate(`primary/metastasis` = Primary_Metastasis) #renaming to make them look cleaner in table

dim(tcga_df) #272 patients with pathology stage data
table(tcga_df$Primary_Metastasis)

#BRD4, age, gender and primary/metastasis status are required
model_os <- coxph(Surv(OS, event = dead) ~ 
                    BRD4+gender+age+`primary/metastasis`+`pathology stage`,
                  data = tcga_df)
ggtable <-ggforest3(model = model_os, 
                    data =  tcga_df,
                    fontsize = 2)
tiff(filename = paste0(file_path,
                       "/20221214/Output/multivariate_cox_os_221215.tiff"),
     width = 4500, height = 3000, units = "px", res = 280)
ggtable
dev.off()


###PART2-------------------------------------------------------------
#MDA cohort
#want to look at  OS and PFS time, age, gender and Mstage.
library(data.table)
library(readxl)
#read in tcga dataframe
mda_df <- read_xlsx(path = paste0(file_path,"20221214/Data/","MDA_cohort_for_multivariant_analysis.xlsx"))
colnames(tcga_df)
?read_tsv()

colnames(mda_df)
table(mda_df$`Vital Status`)
table(mda_df$`Disease Status`)
table(mda_df$Mstage)

mda_df <- mda_df %>%
  mutate(progressed = if_else(is.na(`Date of Progression`),0,1)) %>%
  mutate(dead = if_else(`Vital Status`== "Alive",0,1)) %>%
  mutate(Mstage = toupper(`Disease Status`)) %>%
  mutate(Mstage = if_else(Mstage == "IIIC","M3C",Mstage))  %>% #change IIIC to M3C to be consistent with other 
  #naming, such as "M1B","M1C", etc.
  mutate(Mstage = if_else(Mstage %in% c("M1A","M1B","M1C","M1D"),
                             "M1","M3")) #group to M1 and M3 since M1B only has one patient

dim(mda_df)

model_pfs <- coxph(Surv(`Progression Free Survival`, event = progressed) ~ 
                    Gender+Age+Mstage,
                  data = mda_df)
ggtable <-ggforest3(model = model_pfs, 
                    data =  mda_df,
                    fontsize = 2)

tiff(filename = paste0(file_path,
                       "/20221214/Output/mda_multivariate_cox_pfs_221215.tiff"),
     width = 4500, height = 3000, units = "px", res = 280)
ggtable
dev.off()

model_os <- coxph(Surv(`Overall Survival`, event = dead) ~ 
                    Gender+Age+Mstage,
                  data = mda_df)
ggtable <-ggforest3(model = model_os, 
                    data =  mda_df,
                    fontsize = 2)
tiff(filename = paste0(file_path,
                       "/20221214/Output/mda_multivariate_cox_os_221215.tiff"),
     width = 4500, height = 3000, units = "px", res = 280)
ggtable
dev.off()

#edited 20221216 to include prior treatment info in multivariate analysis of MDA cohort ----------
#"Previous Ipilimumab""Previous BRAF inhibitor""Previous IL-2 or Interferon immunotherapy"Previous chemotherapy"

mda_df_v2 <- read_xlsx(path = paste0(file_path,"20221214/Data/","MDA_cohort_for_multivariant_analysis[7].xlsx"))
colnames(tcga_df)
?read_tsv()

colnames(mda_df_v2)
table(mda_df_v2$`Vital Status`)
table(mda_df_v2$`Disease Status`)
table(mda_df_v2$Mstage)

mda_df_v2 <- mda_df_v2 %>%
  mutate(progressed = if_else(is.na(`Date of Progression`),0,1)) %>%
  mutate(dead = if_else(`Vital Status`== "Alive",0,1)) %>%
  mutate(Mstage = toupper(`Disease Status`)) %>%
  mutate(Mstage = if_else(Mstage == "IIIC","M3C",Mstage))  %>% #change IIIC to M3C to be consistent with other 
  #naming, such as "M1B","M1C", etc.
  mutate(Mstage = if_else(Mstage %in% c("M1A","M1B","M1C","M1D"),
                          "M1","M3")) #group to M1 and M3 since M1B only has one patient

colnames(mda_df_v2)

colnames(mda_df_v2)
#clean up the names otherwise it will mess up with the formatting within ggforest table
mda_df_v2 <- mda_df_v2 %>%
  mutate(`Prior Ipilimumab` = `Previous Ipilimumab`) %>%
  mutate(`Prior BRAF inhibitor` = `Previous BRAF inhibitor`) %>%
  mutate(`Prior IL-2 or IFN immunotherapy` = `Previous IL-2 or Interferon immunotherapy`) %>%
  mutate(`Prior chemotherapy` = `Previous chemotherapy`)

# mda_df_v2 <- mda_df_v2 %>%
#   mutate(`Previous IL-2 or \n Interferon immunotherapy` = `Previous IL-2 or Interferon immunotherapy`)
# # mda_df_v2$`Previous IL-2 or 
# #  Interferon immunotherapy`


mda_df_v2$RECIST
model_pfs <- coxph(Surv(`Progression Free Survival`, event = progressed) ~ 
                     Gender+Age+Mstage+`Prior Ipilimumab`+`Prior BRAF inhibitor`+
                     `Prior IL-2 or IFN immunotherapy`+`Prior chemotherapy`+RECIST,
                   data = mda_df_v2)
ggtable <-ggforest3(model = model_pfs, 
                    data =  mda_df_v2,
                    fontsize = 2)

tiff(filename = paste0(file_path,
                       "/20221214/Output/mda_v2_multivariate_cox_pfs_221215.tiff"),
     width = 6000, height = 3000, units = "px", res = 280)
ggtable
dev.off()

model_os <- coxph(Surv(`Overall Survival`, event = dead) ~ 
                    Gender+Age+Mstage+`Prior Ipilimumab`+`Prior BRAF inhibitor`+
                    `Prior IL-2 or IFN immunotherapy`+`Prior chemotherapy`,
                  data = mda_df_v2)
ggtable <-ggforest(model = model_os, 
                    data =  mda_df_v2,
                    fontsize = 2)
tiff(filename = paste0(file_path,
                       "/20221214/Output/mda_v2_multivariate_cox_os_221215.tiff"),
     width = 6000, height = 3000, units = "px", res = 280)
ggtable
dev.off()






