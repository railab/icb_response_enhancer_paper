---
title: "13_2018-05-24_rGREAT_pathway_analysis"
author: "Ming Tang"
date: "May 24, 2018"
output: html_document
---

```{r}
library(devtools)
#install_github("jokergoo/rGREAT")
library(rGREAT)
library(here)
library(tidyverse)
library(purrr)

```

### read in bed files

```{r}

NRpre_beds<- list.files("results", pattern= "^E7", full.names =T)
Rpre_beds<- list.files("results", pattern = "^Rpre_E7", full.names =T)


NRpre_beds_names<- gsub(".bed", "", basename(NRpre_beds))
Rpre_beds_names<- gsub(".bed", "", basename(Rpre_beds))

NRpre_beds<- setNames(NRpre_beds, NRpre_beds_names)
Rpre_beds<- setNames(Rpre_beds, Rpre_beds_names)

NRpre_beds<- map(NRpre_beds, import, format = "BED")
Rpre_beds<- map(Rpre_beds, import, format = "BED")
```

### submit to GREAT server

```{r}
NRpre_jobs<- map(NRpre_beds, function(x) submitGreatJob(x, rule = "basalPlusExt", request_interval = 30))
Rpre_jobs<- map(Rpre_beds, function(x) submitGreatJob(x, rule = "basalPlusExt", request_interval = 30))

availableOntologies(NRpre_jobs$E7_to_E10)
availableCategories(NRpre_jobs$E7_to_E10)


NRpre_tbs<- map(NRpre_jobs, function(x) getEnrichmentTables(x, ontology = c("MSigDB Pathway")))
Rpre_tbs<- map(Rpre_jobs, function(x) getEnrichmentTables(x, category = c("GO")))
```

mimic the enrichment bar graph:

```{r}


NRpre_E7_to_E10_MF<- NRpre_tbs$E7_to_E10$`GO Molecular Function`
NRpre_E7_to_E10_MSigDB<- NRpre_tbs$E7_to_E10$`MSigDB Pathway`
NRpre_E7_to_E11_MF<- NRpre_tbs$E7_to_E11$`GO Molecular Function`
NRpre_E7_to_E11_MSigDB<- NRpre_tbs$E7_to_E11$`MSigDB Pathway`
NRpre_E7_to_E12_MSigDB<- NRpre_tbs$E7_to_E12$`MSigDB Pathway`

## the author of GREAT responded, only the terms pass both the binom and Hyper test are shown, in addtion, Fold_enrichment should be at least 2
NRpre_E7_to_E10_MF %>% mutate(ranks = rank(Binom_Raw_PValue) ) %>% dplyr::filter(Binom_Adjp_BH <=0.05, Hyper_Adjp_BH <=0.05, Binom_Fold_Enrichment >=2) %>% View()

NRpre_E7_to_E10_MSigDB %>% mutate(ranks = rank(Binom_Raw_PValue) ) %>% dplyr::filter(Binom_Adjp_BH <=0.05, Hyper_Adjp_BH <=0.05, Binom_Fold_Enrichment >=2 ) %>% View()

NRpre_E7_to_E10_MF[1:10,"Binom_Adjp_BH"]
head(NRpre_E7_to_E10_BP)

NRpre_E7_to_E10_MF %>% 
        arrange(Binom_Raw_PValue) %>% 
        View()

NRpre_E7_to_E10_MSigDB %>% 
        arrange(Binom_Raw_PValue) %>% 
        View()

NRpre_E7_to_E10_MSigDB %>% 
        dplyr::filter(grepl("^PID", ID)) %>%
        arrange(Binom_Adjp_BH) %>% 
        View()

NRpre_E7_to_E11_MSigDB %>% 
        dplyr::filter(grepl("^PID", ID)) %>%
        arrange(Binom_Adjp_BH) %>% 
        View()

NRpre_E7_to_E12_MSigDB %>% 
        dplyr::filter(grepl("^PID", ID)) %>%
        arrange(Binom_Adjp_BH) %>% 
        View()

NRpre_E7_to_E10_MSigDB %>% 
        dplyr::filter(grepl("^", ID)) %>%
        arrange(Binom_Adjp_BH) %>% 
        View()

library(ggplot2)
NRpre_E7_to_E10_MF %>% dplyr::filter(Binom_Adjp_BH <=0.05, Hyper_Adjp_BH <=0.05, Binom_Fold_Enrichment >=2) %>%
        ggplot() + 
        geom_bar(aes(x=name, y=-log10(Binom_Raw_PValue)), stat="identity", fill="blue") +
        coord_flip()

```

Need to re-order the name so that the y-value is ordered from high to low:

```{r}
new_order<- order(-log10(NRpre_E7_to_E10_MF$Binom_Raw_PValue), decreasing = FALSE)
NRpre_E7_to_E10_MF$name<- factor(NRpre_E7_to_E10_MF$name, levels = NRpre_E7_to_E10_MF$name[new_order])

NRpre_E7_to_E10_MF %>% dplyr::filter(Binom_Adjp_BH <=0.05, Hyper_Adjp_BH <=0.05, Binom_Fold_Enrichment >=2) %>%
        ggplot() + 
        geom_bar(aes(x=name, y=-log10(Binom_Raw_PValue)), stat="identity", fill="blue") +
        coord_flip()

# or use reorder
NRpre_E7_to_E10_MF %>% dplyr::filter(Binom_Adjp_BH <=0.05, Hyper_Adjp_BH <=0.05, Binom_Fold_Enrichment >=2) %>%
        ggplot() + 
        geom_bar(aes(x=reorder(name, -Binom_Raw_PValue), y=-log10(Binom_Raw_PValue)), stat="identity", fill="blue") +
        xlab("")+
        scale_y_continuous( expand = c(0, 0)) +
        theme_classic(base_size = 14) +
        coord_flip() 
        
```

Association between genomic regions and genes can be get by plotRegionGeneAssociationGraphs(). The function will make the three plots which are same as on GREAT website and returns a GRanges object which contains the gene-region associations.

Which genes are associated with which pathway.

```{r}
par(mfrow = c(1, 3))
res<- plotRegionGeneAssociationGraphs(job, ontology = c("GO Biological Process"), termID="GO:0001570")
plotRegionGeneAssociationGraphs(job)

res1<- plotRegionGeneAssociationGraphs(job, ontology = c("GO Biological Process"), termID="GO:0010574")
View(res)

```


