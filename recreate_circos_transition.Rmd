---
title: "recreate circos"
output: html_document
editor_options: 
  chunk_output_type: console
---

following https://gitlab.com/tangming2005/SKCM_IMT/-/blob/master/scripts/08_2018-05-03_circos_chromHMM_transition.Rmd

```{r}
library(rtracklayer)
library(EnrichedHeatmap)
NRpre<-import("data/chromHMM/NRpre_max.bed", format = "BED")
Rpre<- import("data/chromHMM/Rpre_max.bed", format = "BED")

values(NRpre)
colnames(mcols(NRpre))<- "NRpre"
NRpre$Rpre<- Rpre$name

combine<- NRpre

#window<-  combine[(combine$NRpre == "E9" | combine$Rpre == "E9") & combine$NRpre !="E2" & combine$Rpre != "E2"]

# no interest in the artifacts state 
window<- combine[combine$NRpre !="E13" & combine$Rpre != "E13"]

map = c(
    "E1"  = "Active TSS",
    "E2"  = "Flanking Active TSS",
    "E3"  = "Transcr. at gene 5\' and 3\'",
    "E4"  = "Strong transcription",
    "E5"  = "Weak transcription",
    "E6"  =  "Genic enhancers",
    "E7"  = "Active enhancers",
    "E8"  = "Trivalent/poised TSS",
    "E9"  = "Bivalent/poised TSS",
    "E10" = "Repressed TSS/Enh",
    "E11" = "Repressed Polycomb",
    "E12" = "Low",
    "E13" = "Artifacts"
)


states_col= c(
        "Active TSS"                         = '#ff0000',
        "Flanking Active TSS"                = '#ff4500',
        "Transcr. at gene 5\' and 3\'"       = '#32cd32',
        "Strong transcription"               = '#008000',
        "Weak transcription"                 = '#006400', 
        "Genic enhancers"                    = '#c2e105',
        "Active enhancers"                   = '#ffff00',
        "Trivalent/poised TSS"               = '#cd5c8c',
        "Bivalent/poised TSS"                = '#cd5c5c',
        "Repressed TSS/Enh"                  = '#8a91d0',
        "Repressed Polycomb"                 = '#808080',
        "Low"                                = '#000000',   # black
        "Artifacts"                          = '#ffffff'  # white
        
)


states_name<- names(states_col)
#window$NRpre = map[window$NRpre]
#window$Rpre = map[window$Rpre]
```


```{r}
transition_mat = table(mcols(window)[, c("NRpre", "Rpre")]) * 1000
class(transition_mat) = "matrix"

# reorder to the same order of the states name in the states_col
transition_mat = transition_mat[paste0("E", 1:12), paste0("E", 1:12)]
transition_mat

## percentage of genome does not change
sum(diag(transition_mat))/sum(transition_mat)

# remove the unchanged states
diag(transition_mat) = 0

## this is for lableing the states name in the cord plot
map_reverse<- setNames(names(map),map)
map_reverse

rownames(transition_mat) = paste0("NRpre_", rownames(transition_mat))
colnames(transition_mat) = paste0("Rpre_",  colnames(transition_mat))

```


```{r}


n_states<- 12

library(circlize)
circos.par(gap.after = c(rep(1, n_states - 1), 5, rep(1, n_states - 1), 5))
grid.col = c(states_col[1:12], states_col[1:12])
names(grid.col) = c(rownames(transition_mat), colnames(transition_mat))
chordDiagram(transition_mat, grid.col = grid.col, annotationTrack = c("grid", "axis"),
    directional = TRUE)
circos.clear()
text(0.5, -1, "NRpre")
text(0.5, 1, "Rpre")
legend("left", pch = 15, col = states_col, legend = names(states_col))
```


#### refine the circos plot

Next we set the colors. colmat is the color of the links and the colors are represent as hex code. Links have more transparent (A0) if they contain few transitions (< 70th percentile) because we don't want it to disturb the visualization of the major transitions.

```{r}


pdf("results/NRpre_to_Rpre_transition_circos.pdf", 6, 6)

# one for rows and one for columns
state_col2 = c(states_col[1:12], states_col[1:12])
names(state_col2) = c(rownames(transition_mat), colnames(transition_mat))

colmat = rep(state_col2[rownames(transition_mat)], n_states)
colmat = rgb(t(col2rgb(colmat)), maxColorValue = 255)

qati = quantile(transition_mat, 0.7)
colmat[transition_mat > qati] = paste0(colmat[transition_mat > qati], "A0")
colmat[transition_mat <= qati] = paste0(colmat[transition_mat <= qati], "20")
dim(colmat) = dim(transition_mat)
```

Now we can use chordDiagram() function to make the plot. Here we set one pre-allocated track in which the methylation information will be put.

chordDiagram() returns a data frame which contains coordinates for all links which will be used later.

de is the degree for the “gap” between group 1 and group 2.


```{r}
de = 360 - (360 - 20 - 30) - 30
circos.par(start.degree = -de/4, gap.degree = c(rep(1, n_states-1), de/2, rep(1, n_states-1), de/2))

cdm_res = chordDiagram(transition_mat, col = colmat, grid.col = state_col2,
    directional = TRUE, annotationTrack = "grid", preAllocateTracks = list(track.height = 0.1))
```

If the degree for a sector is larger than 3 degrees, the index for the state and axis is added. Note since there is already one pre-allocated track, the circular rectangles are in the second track (track.index = 2).

```{r}

for(sn in get.all.sector.index()) {
    if(abs(get.cell.meta.data("cell.start.degree", sector.index = sn) - 
           get.cell.meta.data("cell.end.degree", sector.index = sn)) > 3) {
        xcenter = get.cell.meta.data("xcenter", sector.index = sn, track.index = 2)
        ycenter = get.cell.meta.data("ycenter", sector.index = sn, track.index = 2)
        i_state = gsub("(NRpre|Rpre)_", "", sn)
        circos.text(xcenter, ycenter, i_state, col = "white", font = 2, cex = 0.7, 
            sector.index = sn, track.index = 2, adj = c(0.5, 0.5), niceFacing = TRUE)
        circos.axis(sector.index = sn, track.index = 2, major.tick.percentage = 0.2, 
            labels.away.percentage = 0.2, labels.cex = 0.5)
    }
}
```

On the top half, it is easy to see the proportion of different transitions in group 1 that come to every state in group 2. However, it is not straightforward for the states in the bottom half to see the proportion of different states in group 2 they transite to. This can be solved by adding small circular rectangles. In following example, the newly added circular rectangles in the bottom half shows e.g. how much the state 15 in group 1 has been transited to different states in group 2.

```{r}
for(i in seq_len(nrow(cdm_res))) {
    if(cdm_res$value[i] > 0) {
        circos.rect(cdm_res[i, "x1"], -0.5, cdm_res[i, "x1"] - abs(cdm_res[i, "value"]), -0.7, 
            col = state_col2[cdm_res$cn[i]], border = state_col2[cdm_res$cn[i]],
            sector.index = cdm_res$rn[i], track.index = 2)
    }
}

circos.clear()
text(0.5, -1, "NRpre")
text(0.5, 1, "Rpre")
dev.off()

plot.new()
par(xpd=TRUE) # put the legend in a separate page
legend( x = -1.5, y = 1, pch = 15, col = states_col, legend = paste(map_reverse[names(states_col)],names(states_col), sep = " "), cex = 0.75, bty = "n")
par(xpd=FALSE)

pdf("results/NRpre_to_Rpre_transition_circos_legend.pdf", 4, 4)
plot.new()
par(xpd=TRUE) # put the legend in a separate page
legend( "center", pch = 15, col = states_col, legend = paste(map_reverse[names(states_col)],names(states_col), sep = " "), cex = 1, bty = "n")
dev.off()
par(xpd=FALSE)
```


### transition heatmap 

```{r}
library(ComplexHeatmap)

cell_fun = function(j, i, x, y, width, height, fill) {
	grid.rect(x = x, y = y, width = width, height = height, 
		gp = gpar(col = "black", fill = fill, lty = 1, lwd = 0.5))
}

## scale by row
transition_mat_scale<- t(scale(t(transition_mat), center =T, scale =T))
Heatmap(transition_mat_scale, cluster_rows = F, cluster_columns = F)

rownames(transition_mat_scale)<- gsub("NRpre_", "", rownames(transition_mat_scale))
colnames(transition_mat_scale)<- gsub("Rpre_", "", colnames(transition_mat_scale))

col_fun2<- circlize::colorRamp2(c(-2, 0, 3), c("blue", "white", "red"))
pdf("results/NRpre_transit_to_Rpre_scaled_by_NRpre_rows_blue_and_red.pdf", 5, 4)
Heatmap(transition_mat_scale, cluster_rows = F, cluster_columns = F, show_row_names = T, show_column_names = T,
        col = col_fun2, rect_gp = gpar(type = "none"),
        cell_fun = cell_fun,
        name = "relative swtiching\nscore",
        column_title = "Rpre chromatin state (to)",
        column_title_side = "bottom",
        heatmap_legend_param = list(color_bar = "discrete"),
        row_names_side = "left",
        row_title = "NRpre chromatin state (from)")

dev.off()

### Rpre to NRpre

transition_mat2<- t(transition_mat)
## scale by row
transition_mat_scale2<- t(scale(t(transition_mat2), center =T, scale =T))

pdf("results/Rpre_transit_NRpre_scaled_by_Rpre_rows_blue_and_red.pdf", 5, 4)
Heatmap(transition_mat_scale2, cluster_rows = F, cluster_columns = F, show_row_names = T, 
        show_column_names = T,
        col = col_fun2, rect_gp = gpar(type = "none"),
        cell_fun = cell_fun,
        name = "relative swtiching\nscore",
        column_title = "NRpre chromatin state (to)",
        column_title_side = "bottom",
        heatmap_legend_param = list(color_bar = "discrete"),
        row_names_side = "left",
        row_title = "Rpre chromatin state (from)")

dev.off()
```
