#! /bin/bash

set -euo pipefail

file=$1
#the pattern you want to remove from the end of the file name
pattern=$2

sampleName=${file%${pattern}}

## add unique names for each peak for each sample

cat $file | cut -f1-3|  awk -v id="$sampleName" '$4=id"_peak"NR'  OFS="\t" > ${sampleName}_add_name_peaks.bed
