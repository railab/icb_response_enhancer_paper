#!/usr/bin/env python
# coding: utf-8

import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import pandas as pd
import sys
import os
import scipy
import re
import glob
import time
import copy
import seaborn as sns
sns.set_style("whitegrid")

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

kellis_recursive_filelist = glob.glob("/Users/alvinshi/Projects/CancerChIP/MGH/common_MANORM_results/**/*_all_MAvalues.xls",recursive=True)
kellis_recursive_filenames = [os.path.basename(x)[:-17] for x in kellis_recursive_filelist]
kellis_example_df = pd.read_csv(kellis_recursive_filelist[0], sep="\t", index_col=None)

index_names = []
for rowidx, row in kellis_example_df.iterrows():
    new_name = row['chr'] + '_' + str(row['start']) + '_' + str(row['end']) + '_' + str(row['summit'])
    index_names.append(new_name)

M_array = np.zeros((len(index_names),len(kellis_recursive_filenames)))
P_array = np.zeros((len(index_names),len(kellis_recursive_filenames)))
A_array = np.zeros((len(index_names),len(kellis_recursive_filenames)))

idx = 0
start_time = time.time()
summary_stats = {}
for fileidx, filename in zip(kellis_recursive_filelist,kellis_recursive_filenames):
    current_df = pd.read_csv(fileidx, sep="\t", index_col=None)
    summary_stats[filename] = {}
    summary_stats[filename]['MP_pass'] = current_df[(current_df["P_value"] < 0.10) & (current_df["M_value"] > 0)].shape[0]
    summary_stats[filename]['M_pass'] = current_df[current_df["M_value"] > 0].shape[0]
    summary_stats[filename]['P_pass'] = current_df[current_df["P_value"] < 0.10].shape[0]
    summary_stats[filename]['A_pass'] = current_df[current_df["A_value"] > 1].shape[0]
    M_array[:,idx] = current_df.loc[:,"M_value"]
    P_array[:,idx] = current_df.loc[:,"P_value"]
    A_array[:,idx] = current_df.loc[:,"A_value"]
    print("%s done in %0.02f"%(filename, time.time()-start_time))
    idx += 1

summary_stats_df = pd.DataFrame.from_dict(summary_stats).T
summary_stats_df["names"] = summary_stats_df.index.values

passed_MP_filter_summary_stats_df = summary_stats_df[(summary_stats_df["MP_pass"]>0.20*len(index_names))]
passed_MP_filter_summary_stats_df.to_csv("MP_filter_summary_stats_df.csv")

# Manually append records in excel
passed_MP_filter_summary_stats_df = pd.read_csv("MP_filter_summary_stats_df_with_records.csv", index_col=0)
passed_MP_filter_summary_stats_df = passed_MP_filter_summary_stats_df.loc[passed_MP_filter_summary_stats_df["Remove"]==0] 

fig, ax = plt.subplots(figsize=(10,8))
summary_plot = sns.barplot(x="names", y="MP_pass", data=summary_stats_df, ax=ax)
for item in summary_plot.get_xticklabels():
    item.set_rotation(90)
plt.title('MGH Histogram for number of peaks passing M-value and P-value filter', size=20)
plt.xticks(fontsize=14)
plt.yticks(fontsize=16)
plt.xlabel('names', size=16)
plt.ylabel('MP_pass', size=16)
plt.savefig("MGH_MP_pass.pdf",bbox_inches="tight")

passed_MP_filter_summary_stats_df.to_csv("MGH_MP_pass_table.csv")

fig, ax = plt.subplots(figsize=(10,8))
summary_plot = sns.barplot(x="names", y="MP_pass", data=passed_MP_filter_summary_stats_df, ax=ax)
for item in summary_plot.get_xticklabels():
    item.set_rotation(90)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlabel('names', size=16)
plt.ylabel('MP_pass', size=16)
plt.title('MGH Histogram for number of peaks passing M-value and P-value filter', size=20)
plt.savefig("MGH_MP_pass_filtered.pdf",bbox_inches="tight")


mean_M = np.mean(M_array, axis=1)
mean_A = np.mean(A_array, axis=1)
print(np.sum(mean_M > 0))
print(np.sum(mean_M > 0)/len(index_names))
plt.figure(figsize=(10,8))
plt.scatter(mean_A, mean_M, s=0.5)
plt.xlabel("Mean A-values", fontsize=16)
plt.ylabel("Mean M-values", fontsize=16)
plt.title("MGH Mean M vs. A (no filtering for files)", fontsize=20)
plt.savefig("./figures_121822/MGH_MA_nofilter.pdf",bbox_inches="tight")


M_array_df = pd.DataFrame(data = M_array, index = index_names, columns=kellis_recursive_filenames)
P_array_df = pd.DataFrame(data = P_array, index = index_names, columns=kellis_recursive_filenames)
A_array_df = pd.DataFrame(data = A_array, index = index_names, columns=kellis_recursive_filenames)


M_array_sub_df = M_array_df.loc[:,passed_MP_filter_summary_stats_df.index]
P_array_sub_df = P_array_df.loc[:,passed_MP_filter_summary_stats_df.index]
A_array_sub_df = A_array_df.loc[:,passed_MP_filter_summary_stats_df.index]

mean_M_sub_df = np.mean(M_array_sub_df, axis=1)
mean_M_sub_df.to_csv("average_M_MGH_filtered.csv")

mean_A_sub_df = np.mean(A_array_sub_df, axis=1)
mean_A_sub_df.to_csv("average_A_MGH_filtered.csv")

mean_M = np.mean(M_array_sub_df, axis=1)
mean_A = np.mean(A_array_sub_df, axis=1)
print(np.sum(mean_M > 0))
print(np.sum(mean_M > 0)/len(index_names))
plt.figure(figsize=(10,8))
plt.scatter(mean_A, mean_M, s=0.5)
plt.xlabel("Mean A-values", fontsize=16)
plt.ylabel("Mean M-values", fontsize=16)
plt.title("MGH Mean M vs. A (filtered for files)", fontsize=20)
plt.savefig("./figures_121822/MGH_MA_filtered.pdf",bbox_inches="tight")


# Process MDA files
rai_recursive_filelist = glob.glob("/Users/alvinshi/Projects/CancerChIP/MDA/common_MANORM_results/**/*_all_MAvalues.xls",recursive=True)
rai_recursive_filenames = [os.path.basename(x)[:-17] for x in rai_recursive_filelist]
rai_M_array = np.zeros((len(index_names),len(rai_recursive_filenames)))
rai_P_array = np.zeros((len(index_names),len(rai_recursive_filenames)))
rai_A_array = np.zeros((len(index_names),len(rai_recursive_filenames)))

idx = 0
start_time = time.time()
rai_summary_stats = {}
for fileidx, filename in zip(rai_recursive_filelist,rai_recursive_filenames):
    current_df = pd.read_csv(fileidx, sep="\t", index_col=None)
    rai_summary_stats[filename] = {}
    rai_summary_stats[filename]['MP_pass'] = current_df[(current_df["P_value"] < 0.10) & (current_df["M_value"] > 0)].shape[0]
    rai_summary_stats[filename]['M_pass'] = current_df[current_df["M_value"] > 0].shape[0]
    rai_summary_stats[filename]['P_pass'] = current_df[current_df["P_value"] < 0.10].shape[0]
    rai_summary_stats[filename]['A_pass'] = current_df[current_df["A_value"] > 1].shape[0]
    rai_M_array[:,idx] = current_df.loc[:,"M_value"]
    rai_P_array[:,idx] = current_df.loc[:,"P_value"]
    rai_A_array[:,idx] = current_df.loc[:,"A_value"]
    print("%s done in %0.02f"%(filename, time.time()-start_time))
    idx += 1

rai_M_array_df = pd.DataFrame(data = rai_M_array, index = index_names, columns=rai_recursive_filenames)
rai_P_array_df = pd.DataFrame(data = rai_P_array, index = index_names, columns=rai_recursive_filenames)
rai_A_array_df = pd.DataFrame(data = rai_A_array, index = index_names, columns=rai_recursive_filenames)

rai_summary_stats_df = pd.DataFrame.from_dict(rai_summary_stats).T
rai_summary_stats_df["names"] = rai_summary_stats_df.index.values
rai_summary_stats_df.to_csv("MDA_Batch1_MP_Pass_table.csv")


fig, ax = plt.subplots(figsize=(10,8))
summary_plot = sns.barplot(x="names", y="MP_pass", data=rai_summary_stats_df, ax=ax)
for item in summary_plot.get_xticklabels():
    item.set_rotation(90)
plt.title('MDA Histogram for number of peaks passing M-value and P-value filter', size=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlabel('names', size=16)
plt.ylabel('MP_pass', size=16)
plt.savefig("MDA_MP_pass.pdf",bbox_inches="tight")

rai_mean_M = np.mean(rai_M_array, axis=1)
rai_mean_A = np.mean(rai_A_array, axis=1)
print(np.sum(mean_M > 0))
print(np.sum(mean_M > 0)/len(index_names))
plt.figure(figsize=(10,8))
plt.scatter(rai_mean_A, rai_mean_M, s=0.5)
plt.xlabel("Mean A-values", fontsize=16)
plt.ylabel("Mean M-values", fontsize=16)
plt.title("MDA Mean M vs. A", fontsize=20)
plt.savefig("./figures_121822/MDA_MA_nofiltered.pdf",bbox_inches="tight")

rai_mean_M_df = np.mean(rai_M_array_df, axis=1)
rai_mean_M_df.to_csv("average_M_MDA.csv")

rai_mean_A_df = np.mean(rai_A_array_df, axis=1)
rai_mean_A_df.to_csv("average_A_MDA.csv")

rai2_recursive_filelist = glob.glob("/Users/alvinshi/Projects/CancerChIP/MDA2/common_MANORM_results/**/*_all_MAvalues.xls",recursive=True)
rai2_recursive_filenames = [os.path.basename(x)[:-17] for x in rai2_recursive_filelist]

rai2_M_array = np.zeros((len(index_names),len(rai2_recursive_filenames)))
rai2_P_array = np.zeros((len(index_names),len(rai2_recursive_filenames)))
rai2_A_array = np.zeros((len(index_names),len(rai2_recursive_filenames)))

idx = 0
start_time = time.time()
rai2_summary_stats = {}
for fileidx, filename in zip(rai2_recursive_filelist,rai2_recursive_filenames):
    current_df = pd.read_csv(fileidx, sep="\t", index_col=None)
    rai2_summary_stats[filename] = {}
    rai2_summary_stats[filename]['MP_pass'] = current_df[(current_df["P_value"] < 0.10) & (current_df["M_value"] > 0)].shape[0]
    rai2_summary_stats[filename]['M_pass'] = current_df[current_df["M_value"] > 0].shape[0]
    rai2_summary_stats[filename]['P_pass'] = current_df[current_df["P_value"] < 0.10].shape[0]
    rai2_summary_stats[filename]['A_pass'] = current_df[current_df["A_value"] > 1].shape[0]
    rai2_M_array[:,idx] = current_df.loc[:,"M_value"]
    rai2_P_array[:,idx] = current_df.loc[:,"P_value"]
    rai2_A_array[:,idx] = current_df.loc[:,"A_value"]
    print("%s done in %0.02f"%(filename, time.time()-start_time))
    idx += 1


rai2_M_array_df = pd.DataFrame(data = rai2_M_array, index = index_names, columns=rai2_recursive_filenames)
rai2_P_array_df = pd.DataFrame(data = rai2_P_array, index = index_names, columns=rai2_recursive_filenames)
rai2_A_array_df = pd.DataFrame(data = rai2_A_array, index = index_names, columns=rai2_recursive_filenames) 
rai2_summary_stats_df = pd.DataFrame.from_dict(rai2_summary_stats).T
rai2_summary_stats_df["names"] = rai2_summary_stats_df.index.values

fig, ax = plt.subplots(figsize=(10,8))
summary_plot = sns.barplot(x="names", y="MP_pass", data=rai2_summary_stats_df, ax=ax)
for item in summary_plot.get_xticklabels():
    item.set_rotation(90)
summary_plot.axhline(0.20*len(index_names))
plt.title('MDA Batch 2 Histogram for number of peaks passing M-value and P-value filter', size=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlabel('names', size=16)
plt.ylabel('MP_pass', size=16)
plt.savefig("MDA_Batch2_MP_pass.pdf",bbox_inches="tight")

MDA2_passed_MP_filter_summary_stats_df = rai2_summary_stats_df[(rai2_summary_stats_df["MP_pass"]>0.20*len(index_names))]
MDA2_passed_MP_filter_summary_stats_df = rai2_summary_stats_df[(rai2_summary_stats_df["MP_pass"]>0.20*len(index_names))]
MDA2_passed_MP_filter_summary_stats_df.to_csv("MDA2_MP_filter_summary_stats_df.csv")
MDA2_passed_MP_filter_summary_stats_df = pd.read_csv("MDA2_MP_filter_summary_stats_df_with_records.csv", index_col = 0)
MDA2_passed_MP_filter_summary_stats_df.loc[MDA2_passed_MP_filter_summary_stats_df["Keep"] == 1,:].to_csv("MDA2_MP_pass_table.csv")

fig, ax = plt.subplots(figsize=(10,8))
summary_plot = sns.barplot(x="names", y="MP_pass", data=MDA2_passed_MP_filter_summary_stats_df.loc[MDA2_passed_MP_filter_summary_stats_df["Keep"] == 1,:], ax=ax)
for item in summary_plot.get_xticklabels():
    item.set_rotation(90)
plt.title('MDA Batch 2 Histogram for number of peaks passing M-value and P-value filter', size=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.xlabel('names', size=16)
plt.ylabel('MP_pass', size=16)
plt.savefig("MDA_Batch2_MP_pass_filtered.pdf",bbox_inches="tight")

rai2_M_array_sub_df = rai2_M_array_df.loc[:,MDA2_passed_MP_filter_summary_stats_df.index]
rai2_P_array_sub_df = rai2_P_array_df.loc[:,MDA2_passed_MP_filter_summary_stats_df.index]
rai2_A_array_sub_df = rai2_A_array_df.loc[:,MDA2_passed_MP_filter_summary_stats_df.index]
rai2_mean_M_df = np.mean(rai2_M_array_sub_df, axis=1)
rai2_mean_M_df.to_csv("MDA2_average_M_MDA.csv")
rai2_M_array_sub_df.loc[intersect_idr,:].to_csv("Rai2_M_idr_filtered.csv")

# Process IDR values
mda_mgh_idr_df = pd.read_csv("./MGH_MDA1_M_IDR_summary.csv", index_col=0)
mda_mgh_idr_df.columns = ['MGH_M_val', 'MDA_M_val', 'local_idr', 'IDR']
mda_mgh_idr_df["idr_true"] = mda_mgh_idr_df["local_idr"] < 0.01
mda_mgh_idr_sub_df = mda_mgh_idr_df.loc[mda_mgh_idr_df["idr_true"] == True]

from scipy import stats
plt_pearsonr = stats.pearsonr(mda_mgh_idr_df['MGH_M_val'], mda_mgh_idr_df['MDA_M_val'])
print(plt_pearsonr)

mda_mgh_idr_df.loc[mda_mgh_idr_df["idr_true"] == True,:].shape
fig, ax = plt.subplots(figsize=(14,14))
sns.scatterplot(data=mda_mgh_idr_df, x="MGH_M_val", y="MDA_M_val", hue="idr_true", ax=ax, rasterized=True)
plt.xlabel('MGH signal-to-noise ratio (M-value)', size=20)
plt.ylabel('MDA Batch 1 signal-to-noise ratio (M-value)', size=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.title('MGH vs. MDA Batch 1 signal-to-noise ratios mark replicated peaks', size=20)
plt.annotate("pearson = 0.0899", (-4.25,2), fontsize=16)
plt.annotate("N = 50,778", (-4.25,1.75), fontsize=16)
legend = plt.legend(loc="lower right", title="Replicated peaks (idr<0.01)", fontsize=14)
legend.get_title().set_fontsize('14')
legend.get_texts()[0].set_text('Not Replicated')
legend.get_texts()[1].set_text('Replicated')
plt.tight_layout()
plt.savefig("./MGH_vs_MDA1_idr.pdf",bbox_inches="tight", dpi=400)


mda2_mgh_idr_df = pd.read_csv("./MGH_MDA2_M_IDR_summary.csv", index_col=0)
mda2_mgh_idr_df.columns = ['MGH_M_val', 'MDA2_M_val', 'local_idr', 'IDR']
mda2_mgh_idr_df["idr_true"] = mda2_mgh_idr_df["local_idr"] < 0.01
mda2_mgh_idr_sub_df = mda2_mgh_idr_df.loc[mda2_mgh_idr_df["idr_true"] == True]

from scipy import stats
plt_pearsonr = stats.pearsonr(mda2_mgh_idr_df['MGH_M_val'], mda2_mgh_idr_df['MDA2_M_val'])
print(plt_pearsonr)


mda2_mgh_idr_df.loc[mda2_mgh_idr_df["idr_true"] == True,:].shape

fig, ax = plt.subplots(figsize=(14,14))
sns.scatterplot(data=mda2_mgh_idr_df, x="MGH_M_val", y="MDA2_M_val", hue="idr_true", ax=ax, rasterized=True)
plt.xlabel('MGH signal-to-noise ratio (M-value)', size=20)
plt.ylabel('MDA Batch 2 signal-to-noise ratio (M-value)', size=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.title('MGH vs. MDA Batch 2 signal-to-noise ratios mark replicated peaks', size=20)
plt.annotate("pearson = 0.018", (-4,1.35), fontsize=16)
plt.annotate("N = 51,756", (-4,1.15), fontsize=16)
legend = plt.legend(loc="lower right", title="Replicated peaks (idr<0.01)", fontsize=14)
legend.get_title().set_fontsize('14')
legend.get_texts()[0].set_text('Not Replicated')
legend.get_texts()[1].set_text('Replicated')
plt.tight_layout()
plt.savefig("./MGH_vs_MDA2_idr.pdf",bbox_inches="tight", dpi=400)

mda2_mda1_idr_df = pd.read_csv("./MDA1_MDA2_M_IDR_summary.csv", index_col=0)
mda2_mda1_idr_df.columns = ['MDA1_M_val', 'MDA2_M_val', 'local_idr', 'IDR']
mda2_mda1_idr_df["idr_true"] = mda2_mda1_idr_df["local_idr"] < 0.01
mda2_mda1_idr_sub_df = mda2_mda1_idr_df.loc[mda2_mda1_idr_df["idr_true"] == True]

plt_pearsonr = stats.pearsonr(mda2_mda1_idr_df['MDA1_M_val'], mda2_mda1_idr_df['MDA2_M_val'])
fig, ax = plt.subplots()
ax.text(0.5, 0.5, f'Pearson correlation: {plt_pearsonr[0]:.4f}\nP-value: {plt_pearsonr[1]:.4e}', 
        fontsize=12, ha='center')
fig.savefig('pearsonr_results.pdf', bbox_inches="tight", dpi=400)

mda2_mda1_idr_df.loc[mda2_mda1_idr_df["idr_true"] == True,:].shape

fig, ax = plt.subplots(figsize=(14,14))
sns.scatterplot(data=mda2_mda1_idr_df, x="MDA1_M_val", y="MDA2_M_val", hue="idr_true", ax=ax, rasterized=True)
plt.xlabel('MDA Batch 1 signal-to-noise ratio (M-value)', size=20)
plt.ylabel('MDA Batch 2 signal-to-noise ratio (M-value)', size=20)
plt.xticks(fontsize=16)
plt.yticks(fontsize=16)
plt.title('MDA Batch 1 vs. MDA Batch 2 signal-to-noise ratios mark replicated peaks', size=20)
plt.annotate("pearson = 0.6525", (-4,1.35), fontsize=16)
plt.annotate("N = 50,856", (-4,1.15), fontsize=16)
legend = plt.legend(loc="lower right", title="Replicated peaks (idr<0.01)", fontsize=14)
legend.get_title().set_fontsize('14')
legend.get_texts()[0].set_text('Not Replicated')
legend.get_texts()[1].set_text('Replicated')
plt.tight_layout()
plt.savefig("./MDA1_vs_MDA2_idr.pdf",bbox_inches="tight", dpi=400)

intersection_set = set(mda2_mgh_idr_sub_df.index.values).union(mda_mgh_idr_sub_df.index.values)
intersection_set = set(intersection_set).union(mda2_mgh_idr_sub_df.index.values)
rai2_M_array_sub_df.loc[intersection_set,:].to_csv("Rai2_M_idr_filtered.csv")
rai_M_array_df.loc[intersection_set,:].to_csv("MDA_Rai_M_idr_filtered.csv")
M_array_sub_df.loc[intersection_set,:].to_csv("MDA_Kellis_M_idr_filtered.csv")